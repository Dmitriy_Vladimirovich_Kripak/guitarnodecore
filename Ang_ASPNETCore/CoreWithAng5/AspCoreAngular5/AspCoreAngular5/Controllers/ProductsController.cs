﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AspCoreAngular5.Models;

namespace AspCoreAngular5.Controllers
{
    [Produces("application/json")]
    [Route("api/Products")]
    public class ProductsController : Controller
    {
        ApplicationContext db;

        public ProductsController(ApplicationContext context)
        {
            db = context;
            if(db.Products.Any())
            {
                db.Products.Add(new Products { Name = "iPhone X", Company = "Apple", Price = 79900 });
                db.Products.Add(new Products { Name = "Galaxy S8", Company = "Samsung", Price = 49900 });
                db.Products.Add(new Products { Name = "Pixel 2", Company = "Google", Price = 52900 });
                db.SaveChanges();
            }
        }

        [HttpGet]
        public IEnumerable<Products> Get()
        {
            return db.Products.ToList();
        }

        [HttpGet("{id}")]
        public Products Get(int id)
        {
            Products product = db.Products.FirstOrDefault(x => x.Id == id);
            return product;
        }

        [HttpPost]
        public IActionResult Post([FromBody]Products product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return Ok(product);
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Products product)
        {
            if (ModelState.IsValid)
            {
                db.Update(product);
                db.SaveChanges();
                return Ok(product);
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Products product = db.Products.FirstOrDefault(x => x.Id == id);
            if (product != null)
            {
                db.Products.Remove(product);
                db.SaveChanges();
            }
            return Ok(product);
        }
    }
}