﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;

namespace ConsoleAppTest.DAL
{
    public class UsersData : DbContext 
    {
        public DbSet<User> Users { get; set; }

        public UsersData() : base("Users")
        {

        }
    }
}
