﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using 

namespace MVC_InestigationZERONETApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            EmbeddedResourcePathConfiguration configuration =
                    new EmbeddedResourcePathConfiguration(
                        rootNameSpace: "HtmlViewTemplate",
                        viewFolderName: "Views",
                        resourceAssembly: Assembly.Load("HtmlViewTemplate"));
            HostingEnvironment.RegisterVirtualPathProvider(new EmbeddedViewPathProvider(configuration));
        }
    }
}
