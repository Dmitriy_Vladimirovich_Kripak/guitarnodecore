using Abp.AutoMapper;
using Test.Editions;
using Test.MultiTenancy.Payments.Dto;

namespace Test.Web.Areas.App.Models.SubscriptionManagement
{
    [AutoMapTo(typeof(ExecutePaymentDto))]
    public class PaymentResultViewModel : SubscriptionPaymentDto
    {
        public EditionPaymentType EditionPaymentType { get; set; }
    }
}