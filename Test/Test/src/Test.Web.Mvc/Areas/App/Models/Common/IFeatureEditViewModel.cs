using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Test.Editions.Dto;

namespace Test.Web.Areas.App.Models.Common
{
    public interface IFeatureEditViewModel
    {
        List<NameValueDto> FeatureValues { get; set; }

        List<FlatFeatureDto> Features { get; set; }
    }
}