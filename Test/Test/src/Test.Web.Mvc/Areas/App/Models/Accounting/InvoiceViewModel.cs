﻿using Test.MultiTenancy.Accounting.Dto;

namespace Test.Web.Areas.App.Models.Accounting
{
    public class InvoiceViewModel
    {
        public InvoiceDto Invoice { get; set; }
    }
}
