﻿using System.Collections.Generic;
using Test.Caching.Dto;

namespace Test.Web.Areas.App.Models.Maintenance
{
    public class MaintenanceViewModel
    {
        public IReadOnlyList<CacheDto> Caches { get; set; }
    }
}