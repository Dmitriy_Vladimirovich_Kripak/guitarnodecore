﻿using Test.Sessions.Dto;

namespace Test.Web.Areas.App.Models.Editions
{
    public class SubscriptionDashboardViewModel
    {
        public GetCurrentLoginInformationsOutput LoginInformations { get; set; }
    }
}
