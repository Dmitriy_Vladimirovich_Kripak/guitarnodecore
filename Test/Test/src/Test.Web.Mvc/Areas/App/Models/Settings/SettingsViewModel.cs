﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Test.Configuration.Tenants.Dto;

namespace Test.Web.Areas.App.Models.Settings
{
    public class SettingsViewModel
    {
        public TenantSettingsEditDto Settings { get; set; }
        
        public List<ComboboxItemDto> TimezoneItems { get; set; }
    }
}