﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Test.Configuration.Host.Dto;
using Test.Editions.Dto;

namespace Test.Web.Areas.App.Models.HostSettings
{
    public class HostSettingsViewModel
    {
        public HostSettingsEditDto Settings { get; set; }

        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }

        public List<ComboboxItemDto> TimezoneItems { get; set; }
    }
}