﻿using System.Collections.Generic;
using Test.Editions.Dto;

namespace Test.Web.Areas.App.Models.Tenants
{
    public class TenantIndexViewModel
    {
        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }
    }
}