﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Test.Web.Areas.App.Models.Layout;
using Test.Web.Session;
using Test.Web.Views;

namespace Test.Web.Areas.App.Views.Shared.Components.AppFooter
{
    public class AppFooterViewComponent : TestViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AppFooterViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var footerModel = new FooterViewModel
            {
                LoginInformations = await _sessionCache.GetCurrentLoginInformationsAsync()
            };

            return View(footerModel);
        }
    }
}
