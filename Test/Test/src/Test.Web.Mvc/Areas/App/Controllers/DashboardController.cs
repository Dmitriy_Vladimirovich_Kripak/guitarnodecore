﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using Test.Authorization;
using Test.Web.Controllers;

namespace Test.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Dashboard)]
    public class DashboardController : TestControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}