using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using Test.Web.Controllers;

namespace Test.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize]
    public class WelcomeController : TestControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}