﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Test.Authorization;
using Test.Web.Controllers;

namespace Test.Web.Mvc.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_UiCustomization)]
    public class MyFirstController : TestControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}