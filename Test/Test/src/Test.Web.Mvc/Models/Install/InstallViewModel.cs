﻿using System.Collections.Generic;
using Abp.Localization;
using Test.Install.Dto;

namespace Test.Web.Models.Install
{
    public class InstallViewModel
    {
        public List<ApplicationLanguage> Languages { get; set; }

        public AppSettingsJsonDto AppSettingsJson { get; set; }
    }
}
