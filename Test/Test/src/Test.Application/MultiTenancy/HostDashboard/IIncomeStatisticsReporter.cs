﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Test.MultiTenancy.HostDashboard.Dto;

namespace Test.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}