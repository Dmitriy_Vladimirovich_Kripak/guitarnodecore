namespace Test.MultiTenancy.Payments.Dto
{
    public class GetSubscriptionPaymentInput
    {
        public long Id { get; set; }
    }
}