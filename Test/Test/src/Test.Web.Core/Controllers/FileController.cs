﻿using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;
using Test.Dto;
using Test.Storage;

namespace Test.Web.Controllers
{
    public class FileController : TestControllerBase
    {
        private readonly ITempFileCacheManager _tempFileCacheManager;

        public FileController(ITempFileCacheManager tempFileCacheManager)
        {
            _tempFileCacheManager = tempFileCacheManager;
        }

        [DisableAuditing]
        public ActionResult DownloadTempFile(FileDto file)
        {
            var fileBytes = _tempFileCacheManager.GetFile(file.FileToken);
            if (fileBytes == null)
            {
                return NotFound(L("RequestedFileDoesNotExists"));
            }

            return File(fileBytes, file.FileType, file.FileName);
        }
    }
}