using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Test.Editions.Dto;

namespace Test.MultiTenancy.Dto
{
    public class GetTenantFeaturesEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}