﻿using Abp.Application.Services;
using Test.Dto;
using Test.Logging.Dto;

namespace Test.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
