﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp;
using Abp.Authorization.Users;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;

namespace Test.MyFirst
{
    [Table("AppMyFirst")]
    public class MyFirst : IHasCreationTime, IMayHaveTenant
    {
        public Guid Id { get; set; }

        public long UserId { get; set; }

        public string MyName { get; set; }

        public DateTime CreationTime { get; set; }

        public int? TenantId { get; set; }
    }
}
