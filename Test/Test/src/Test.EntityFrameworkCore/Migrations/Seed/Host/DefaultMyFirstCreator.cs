﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Test.EntityFrameworkCore;

namespace Test.Migrations.Seed.Host
{
    class DefaultMyFirstCreator
    {
        private readonly TestDbContext context;

        public DefaultMyFirstCreator(TestDbContext _context)
        {
            context = _context;
        }

        public void Create()
        {
            var Dima = context.MyFirsts.FirstOrDefault(m => m.MyName == "Dima");

            if (Dima == null)
            {
                context.MyFirsts.Add(
                    new MyFirst.MyFirst
                    {
                        MyName = "Dima",
                        CreationTime = DateTime.Now,
                        TenantId = 1,
                        UserId = 1
                    });
            }
        }
    }
}
