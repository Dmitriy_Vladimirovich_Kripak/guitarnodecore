﻿using System;
using System.Data.Entity;


namespace AngularGridTestApp.Controllers
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public ApplicationContext() : base("UsersInformation")
        {

        }
    }


    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}