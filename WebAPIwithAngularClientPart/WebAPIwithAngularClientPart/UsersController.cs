﻿using AngularGridTestApp;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;

namespace AngularGridTestApp.Controllers
{
    public class UsersController : ApiController
    {
        ApplicationContext db = new ApplicationContext();

        public IHttpActionResult Get()
        {
            return Ok(db.Users.ToList());
        }

        public IHttpActionResult Get(int id)
        {
            User user = db.Users.FirstOrDefault(x => x.Id == id);
            if (user != null)
                return Ok(user);

            return NotFound();
        }

        public IHttpActionResult Post([FromBody]User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
            return Ok(user);
        }

        public IHttpActionResult Put(int id, [FromBody]User user)
        {
            User u = db.Users.Find(id);
            if (u != null)
            {
                u.Name = user.Name;
                u.Age = user.Age;
                db.Entry(u).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Ok(u);
            }
            return NotFound();
        }

        public IHttpActionResult Delete(int id)
        {
            User user = db.Users.Find(id);
            if (user != null)
            {
                db.Users.Remove(user);
                db.SaveChanges();
                return Ok(user);
            }

            return NotFound();
        }
    }

}