﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace OneToOne
{
    class User
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    class UserBlock
    {
        public int Id { get; set; }

        public bool IsBlocked { get; set; }
    }

    class Context : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<UserBlock> UserBlocks { get; set; }
    }
}
